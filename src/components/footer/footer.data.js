import facebook from 'assets/images/icons/facebook.png';
import twitter from 'assets/images/icons/twitter.png';
import github from 'assets/images/icons/github.png';
import dribbble from 'assets/images/icons/dribbble.png';
import linkedin from 'assets/images/icons/linkedin.png';
import phone from 'assets/images/icons/telephone.png';
import location from 'assets/images/icons/location.png';
import email from 'assets/images/icons/email.png';

export const menuItems = [
  {
    id: 2,
    title: 'About Us',
    items: [
      {
        path: '#!',
        label: 'Customer Support',
      },
      {
        path: '#!',
        label: 'About Us',
      },
      {
        path: '#!',
        label: 'Copyright',
      },
    ],
  },
  {
    id: 3,
    title: 'Contact',
    items: [
      {
        path: '#!',
        icon: phone,
        label: '+91 8686351929',
      },
      {
        path: '#!',
        icon: email,
        label: 'lohithkumar.gangadevi@levybot.tech',
      },
      {
        path: '#!',
        icon: location,
        label: 'Hyderabad - Telangana 500032',
      }
    ],
  },
  {
    id: 4,
    title: 'Connect',
    items: [
      {
        path: 'https://www.linkedin.com/company/levybot',
        icon: linkedin,
        label: 'LinkedIn',
      },
    ],
  },
];

export const footerNav = [
  {
    path: '#!',
    label: 'Home',
  },
  {
    path: '#!',
    label: 'Advertise',
  },
  {
    path: '#!',
    label: 'Supports',
  },
  {
    path: '#!',
    label: 'Marketing',
  },
  {
    path: '#!',
    label: 'FAQ',
  },
];
