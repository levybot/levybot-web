/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx, Text } from 'theme-ui';
import { Link } from 'components/link';
import logo from 'assets/images/levybot-logo.png'
import Image from 'next/image'

export default function Logo({ isSticky, footer, ...props }) {
  return (
    <Link path="/" sx={styles.logo} {...props}>
      <Image
        src={logo}
        alt="LevyBot Logo"
        width={50}
        height={50}
      />
      <Text 
        sx={{
          fontSize: 22,
          fontWeight: 'bold',
          marginLeft: 2,
          color: 'white'
        }}
      >LevyBot</Text>
    </Link>
  );
}
const styles = {
  logo: {
    alignItems: 'center',
    cursor: 'pointer',
    display: 'inline-flex',
    svg: {
      height: 'auto',
      width: [128, null, '100%'],
    },
  },
};
