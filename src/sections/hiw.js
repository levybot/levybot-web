import { Container, Grid, Typography } from '@mui/material'
import { Box } from '@mui/system'
import React from 'react'

import { createTheme, ThemeProvider } from '@mui/material/styles';


const theme = createTheme();

theme.typography.h3 = {
  fontSize: '3rem',
  color: 'white',
  fontWeight: 'bold',
  fontFamily: 'DM Sans',
  '@media (min-width:280px)': {
    fontSize: '1.5rem',
    fontWeight: 'bold',
    fontFamily: 'DM Sans',
  },
  [theme.breakpoints.up('md')]: {
    fontSize: '3rem',
    fontWeight: 'bold',
  },
};

theme.typography.h4 = {
    fontSize: '2rem',
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'DM Sans',
    '@media (min-width:280px)': {
      fontSize: '1.2rem',
      fontWeight: 'bold',
      fontFamily: 'DM Sans',
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '2rem',
      fontWeight: 'bold',
    },
  };

function hiw() {
    return (
        <>
        <Box id="hiw" sx={styles.section}>
            <ThemeProvider theme={theme}>
                <Container>
                   <Typography variant='h3'>How Levybot can help speed up the PR review process</Typography>
                    <Grid container sx={{ mt: 4 }} display={'flex'} justifyContent={'center'}>
                            <Grid item xs="6">
                            
                            </Grid>
                            <Grid item xs="6">
                                <Typography sx={{ fontFamily: 'DM Sans',  fontWeight: '500', color: 'white' }} variant='h4'>Integrete with GIT</Typography>
                                <Typography component={"p"} variant='subtitle1' sx={{ fontFamily: 'DM Sans',  fontWeight: '400', color: 'white' }}>Authorize with GIT provider using the credentials.</Typography>
                            </Grid>
                
                        
                            <Grid item xs="6">
                                <Typography sx={{ fontFamily: 'DM Sans',  fontWeight: '500', color: 'white' }} variant='h4'>Connect respository</Typography>
                                <Typography component={"p"} variant='subtitle1' sx={{ fontFamily: 'DM Sans',  fontWeight: '400', color: 'white' }}>Select the respository for PR review process.</Typography>
                            </Grid>
                            <Grid item xs="6">
                            
                            </Grid>
                
                            <Grid item xs="6">
                            
                            </Grid>
                            <Grid item xs="6">
                            <Typography sx={{ fontFamily: 'DM Sans',  fontWeight: '500', color: 'white' }} variant='h4'>Levybot magic</Typography>
                                <Typography component={"p"} variant='subtitle1' sx={{ fontFamily: 'DM Sans',  fontWeight: '400', color: 'white' }}>Let levybot reviews all the pull requests and auto notify changes to reviewer before merging into master.</Typography>
                            </Grid>
                    </Grid>
                </Container>
            </ThemeProvider> 
        </Box>
        </>
    )
}

    export default hiw

    const styles = {
        section: {
          py: 10,
          backgroundColor: '#2d3436',
          backgroundImage: 'linear-gradient(315deg, #2d3436 0%, #000000 74%)'
        }
    };