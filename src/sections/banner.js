
import { Container, Grid, Box, Typography } from '@mui/material'
import Image from 'next/image'

import { createTheme, ThemeProvider } from '@mui/material/styles';

import heropattern from 'assets/images/patterns/hero-pattern.svg'

const theme = createTheme();

theme.typography.h3 = {
  fontSize: '4rem',
  color: 'white',
  fontWeight: 'bold',
  fontFamily: 'DM Sans',
  '@media (min-width:280px)': {
    fontSize: '2rem',
    fontWeight: 'bold',
    fontFamily: 'DM Sans',
  },
  [theme.breakpoints.up('md')]: {
    fontSize: '3rem',
    fontWeight: 'bold',
  },
};

const Banner = () => {
  return (
    <Box sx={styles.section}>
      <Container>
        <ThemeProvider theme={theme}>
          {/* <div style={{ position: 'absolute', left: -10, top: 580 }}>
            <Image
              src={heropattern}
              alt="Picture of the author"
              width="118px"
              height="118px"
            />
          </div> */}
          <Grid 
              container
              direction="column"
              alignItems="center"
              justifyContent="center"
              sx={{ py: 10 }}
            >
              <Grid item xs={12}>
                <Typography align='center' sx={{ mb: 2 }} variant='h3'>Review PR's using the <br /> power of ML/AI</Typography>
                <Image
                  src="/levybot-flow-final1.png"
                  alt="Picture of the author"
                  width="700px"
                  height="250px"
                />
              </Grid>
          </Grid>  
        </ThemeProvider>
      </Container>
    </Box>
  );
};

export default Banner;

const styles = {
  section: {
    position: 'realtive',
    backgroundColor: '#2d3436',
    backgroundImage: 'linear-gradient(315deg, #2d3436 0%, #000000 74%)'
  }
};
