
import { Box, Container, Grid } from '@mui/material'

import ml from 'assets/images/machine-learning.png';
import shield from 'assets/images/shield.png';
import opinion from 'assets/images/opinion.png';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

import Image from 'next/image'

const data = [
  {
    id: 1,
    icon: ml,
    title: 'PR APPROVAL',
    description: `Levybot automates the process of PR reviews before merging into master`,
  },
  {
    id: 2,
    icon: shield,
    title: 'SECURITY',
    description: `levybot can caught any secure keys that are pushed into feature branches`,
  },
  {
    id: 3,
    icon: opinion,
    title: 'AUTO SUGGESTIONS',
    description: `It suggests the coding standards that are used by programmers around the world`,
  },
];

const Services = () => {
  return (
    <Box sx={{ padding: '4em 0em',  mt: -12 }}>
      <Container>
         <Grid container spacing={2}>
           { data.map((item, key) => (
               <Grid item xs={12} sm={6} lg={4} md={6} key={key}>
                    <Card sx={{ p: 2 }} elevation={4}>
                      <CardContent>
                        <Image src={item.icon} height="50" width={"50"} />
                        <Typography sx={{ fontSize: 18, mt: 1, fontWeight: 'bold',  fontFamily: 'DM Sans',}} color="text.primary" gutterBottom>
                          { item.title }
                        </Typography>
                        <Typography sx={{  fontFamily: 'DM Sans' }} variant="body2">
                            {  item.description }
                        </Typography>
                      </CardContent>
                    </Card>
               </Grid>
           )) }
         </Grid>
      </Container>
    </Box>
  );
};

export default Services;
