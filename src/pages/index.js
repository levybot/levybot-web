import React from 'react';
import { ThemeProvider } from 'theme-ui';
import theme from 'theme';
import SEO from 'components/seo';
import Layout from 'components/layout';
import Banner from 'sections/banner';
import Services from 'sections/services';
import SubscribeUs from 'sections/subscribe-us';
import HowItWorks from 'sections/hiw'
import Blog from 'sections/blog';
// import { ThemeProvider } from '@mui/material/styles'
// import theme from 'theme/mui-theme'

export default function IndexPage() {
  return (
    <ThemeProvider theme={theme}>
      <Layout>
        <SEO
          title="LevyBot - An AI/ML Company"
          description="Automate PR approval process using the power of ML/AI"
        />
        <Banner />
        <Services />
        <HowItWorks />
        <Blog />
        <SubscribeUs />
      </Layout>
    </ThemeProvider>
  );
}
