import { createTheme } from '@mui/material/styles';

const theme = createTheme({
    typography: {
        h3: {
            fontSize: '4rem',
            color: 'white',
            fontWeight: 'bold',
            fontFamily: 'DM Sans',
            '@media (min-width:350px)': {
              fontSize: '2rem',
              fontWeight: 'bold',
              fontFamily: 'DM Sans',
            }
        }
    }
})

export default theme